package com.oc.game;

import org.apache.logging.log4j.LogManager;

import java.io.FileOutputStream;
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Properties;
import java.util.Scanner;

public class Plusoumoinsattaque {
    Scanner sc = new Scanner(System.in);
    private static final org.apache.logging.log4j.Logger Logger = LogManager.getLogger();
    Config Config = new Config();
    Properties prop = new Properties();
   // FileOutputStream filestream = null;
    int longCombi= Integer.getInteger(prop.getProperty("db.longueurCombinasion"));
    boolean verifdeveloppeur = Boolean.getBoolean(prop.getProperty("db.modeDeveloppeur"));

    /**
     * class PLusoumoinsattaque jeu en mode attaque le joueur fais des propositions(reponses).
     *
     *     @method  void attaque methode pour le mode 1 attaque  ou le joueur fait des propositions(reponses) avec en args tabsolutionforce,resultat,tourjeu1
     *
     *     @variable int tourjeu1  sert a definir le nombre de tour que se deroule la partie
     *     @variable  int[] tabsolutionforce est la combinastion secrete.
     *     @variable  String[] resultat est le resultat d'attaque.
     *     @variable  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *     @variable String reponse sert au joeur à mettre sa reponse pour devine  la tabsolutionforce.
     *
     *     @author thomas
     *
     */
    public void attaque(int[] tabsolutionforce, String[] resultat, int tourjeu1) {

        if (tourjeu1 == 0) {
            Logger.info("le joueur commence a joue et choisi le mode developpeur ou non");
            System.out.println("Bonjour voulez-vous jouer au mode developpeur ?");
            System.out.println("Si oui vous mettrez 'o' non 'n' ");
            char modedeveloppeur = sc.nextLine().charAt(0);

            while (modedeveloppeur == 'o'){
                System.out.println("le mode developpeur est activé");
                verifdeveloppeur = true;
                System.out.println(tabsolutionforce);
                System.out.println("--------------------------------");
                System.out.println("Bienvenue dans le jeu 1 mode  attaquant sous cette phrase. Vous pouvez entrez votre premiere propostion");
            }
            while (modedeveloppeur == 'n') {
                System.out.println("Vous jourez sans le mode developpeur");
                System.out.println("---------------------------------");
                System.out.println("Bienvenue dans le jeu 1 mode  attaquant sous cette phrase. Vous pouvez entrez votre premiere propostion");
            }
            while ((modedeveloppeur != 'n') && (modedeveloppeur != 'o')) {
                System.out.println("vous n'avez pas rentrer une proposition valide.");
            }
            }

        Logger.info("le joueur fait une proposition");
        System.out.print("Vous avez tentez : ");
            String reponse = sc.nextLine();
            while (reponse.length() < longCombi || reponse.length() > longCombi ) {
                if(reponse.length() < longCombi){
                    System.out.println("vous n'avez pas assez de caractére pour votre reponse");
                }
                if(reponse.length() > longCombi){
                    System.out.println("vous avez trop de caractére pour votre reponse");
                }
            }
            while((Character.getNumericValue(reponse.charAt(0)) != 1) && (Character.getNumericValue(reponse.charAt(0)) != 2) && (Character.getNumericValue(reponse.charAt(0)) != 3) && (Character.getNumericValue(reponse.charAt(0)) != 4) && (Character.getNumericValue(reponse.charAt(0)) != 5) && (Character.getNumericValue(reponse.charAt(0)) != 6) &&(Character.getNumericValue(reponse.charAt(0)) != 7) && (Character.getNumericValue(reponse.charAt(0)) != 8) && (Character.getNumericValue(reponse.charAt(0)) != 9)){
            System.out.println("vous n'avez pas rempli un caratére autorisé pour le permiere caractére");
        }
        while((Character.getNumericValue(reponse.charAt(1)) != 1) && (Character.getNumericValue(reponse.charAt(1)) != 2) && (Character.getNumericValue(reponse.charAt(1)) != 3) && (Character.getNumericValue(reponse.charAt(1)) != 4) && (Character.getNumericValue(reponse.charAt(1)) != 5) && (Character.getNumericValue(reponse.charAt(0)) != 6) &&  (Character.getNumericValue(reponse.charAt(1)) != 7) && (Character.getNumericValue(reponse.charAt(1)) != 8) && (Character.getNumericValue(reponse.charAt(1)) != 9)){
            System.out.println("vous n'avez pas rempli un caratére autorisé pour le deuxieme caractére");
        }
        while((Character.getNumericValue(reponse.charAt(2)) != 1) && (Character.getNumericValue(reponse.charAt(2)) != 2) && (Character.getNumericValue(reponse.charAt(2)) != 3) && (Character.getNumericValue(reponse.charAt(2)) != 4) && (Character.getNumericValue(reponse.charAt(2)) != 5) && (Character.getNumericValue(reponse.charAt(2)) != 6)  && (Character.getNumericValue(reponse.charAt(2)) != 7) && (Character.getNumericValue(reponse.charAt(2)) != 8) && (Character.getNumericValue(reponse.charAt(2)) != 9)){
            System.out.println("vous n'avez pas rempli un caratére autorisé pour le troiseme caractére");
        }
        while((Character.getNumericValue(reponse.charAt(3)) != 1) && (Character.getNumericValue(reponse.charAt(3)) != 2) && (Character.getNumericValue(reponse.charAt(3)) != 3) && (Character.getNumericValue(reponse.charAt(3)) != 4) && (Character.getNumericValue(reponse.charAt(3)) != 5) && (Character.getNumericValue(reponse.charAt(3)) != 6)  && (Character.getNumericValue(reponse.charAt(3)) != 7) && (Character.getNumericValue(reponse.charAt(3)) != 8) && (Character.getNumericValue(reponse.charAt(3)) != 9)){
            System.out.println("vous n'avez pas rempli un caratére autorisé pour le quatrieme caractére");
        }
        while((Character.getNumericValue(reponse.charAt(0)) != 1) && (Character.getNumericValue(reponse.charAt(0)) != 2) && (Character.getNumericValue(reponse.charAt(0)) != 3) && (Character.getNumericValue(reponse.charAt(0)) != 4) && (Character.getNumericValue(reponse.charAt(0)) != 5) && (Character.getNumericValue(reponse.charAt(0)) != 6) && (Character.getNumericValue(reponse.charAt(0)) != 7) && (Character.getNumericValue(reponse.charAt(0)) != 8) && (Character.getNumericValue(reponse.charAt(0)) != 9) || (Character.getNumericValue(reponse.charAt(1)) != 1) && (Character.getNumericValue(reponse.charAt(1)) != 2) && (Character.getNumericValue(reponse.charAt(1)) != 3) && (Character.getNumericValue(reponse.charAt(1)) != 4) && (Character.getNumericValue(reponse.charAt(1)) != 5) && (Character.getNumericValue(reponse.charAt(0)) != 6) &&  (Character.getNumericValue(reponse.charAt(1)) != 7) && (Character.getNumericValue(reponse.charAt(1)) != 8) && (Character.getNumericValue(reponse.charAt(1)) != 9) || (Character.getNumericValue(reponse.charAt(2)) != 1) && (Character.getNumericValue(reponse.charAt(2)) != 2) && (Character.getNumericValue(reponse.charAt(2)) != 3) && (Character.getNumericValue(reponse.charAt(2)) != 4) && (Character.getNumericValue(reponse.charAt(2)) != 5) && (Character.getNumericValue(reponse.charAt(2)) != 6)  && (Character.getNumericValue(reponse.charAt(2)) != 7) && (Character.getNumericValue(reponse.charAt(2)) != 8) && (Character.getNumericValue(reponse.charAt(2)) != 9) || (Character.getNumericValue(reponse.charAt(3)) != 1) && (Character.getNumericValue(reponse.charAt(3)) != 2) && (Character.getNumericValue(reponse.charAt(3)) != 3) && (Character.getNumericValue(reponse.charAt(3)) != 4) && (Character.getNumericValue(reponse.charAt(3)) != 5) && (Character.getNumericValue(reponse.charAt(3)) != 6)  && (Character.getNumericValue(reponse.charAt(3)) != 7) && (Character.getNumericValue(reponse.charAt(3)) != 8) && (Character.getNumericValue(reponse.charAt(3)) != 9)){
            System.out.println("vous devez rentrer une nouvelle proposition");
            reponse = sc.nextLine();
        }
                for (int tour = 0; tour < 4; tour++) {
                    if (Character.getNumericValue(reponse.charAt(tour)) == tabsolutionforce[tour]) {
                        resultat[tour] = "=";
                    }
                    if (Character.getNumericValue(reponse.charAt(tour)) > tabsolutionforce[tour]) {
                        resultat[tour] = "-";
                    }
                    if (Character.getNumericValue(reponse.charAt(tour)) < tabsolutionforce[tour]) {
                        resultat[tour] = "+";
                    }
                    //   if (Character.getNumericValue(reponse.charAt(tour)) > 0) {
                    //       throw new ArithmeticException("ce n'est pas un chiffre compris entre 1 et 9 ou c'est un caratere interdit");
                    //    }
                    // boolean correct = true;
                }
                Logger.info("la variable du resultat joueur est affiché " + Arrays.toString(resultat));
                System.out.println("Résultat : " + Arrays.toString(resultat));
                System.out.println("---------------------");
            }
    }

