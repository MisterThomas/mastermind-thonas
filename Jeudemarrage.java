package com.oc.game;

import org.apache.logging.log4j.LogManager;

import java.io.FileOutputStream;
import java.util.Properties;
import java.util.Scanner;

public class Jeudemarrage {
    Config Config = new Config();
    Properties prop = new Properties();
    //FileOutputStream filestream = null;
    Scanner sc = new Scanner(System.in);
    private static final org.apache.logging.log4j.Logger Logger = LogManager.getLogger();

    /**
     * class Jeudemarrage sert a rejouer à un des jeux apres une victoire ou defaite.
     *
     *      @method   Choixjeu.Choixjeugame sert a un lancer un jeu parmi les trois methodes et classe des trois jeux 1 attaque, 2 defense, 3 duel avec toute les variable en args en dessous
     *
     *       @variable int[] tabsolutionforce est la combinastion secrete.
     *       @variable  String[] resultat est le resultat d'attaque.
     *       @variable  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *       @variable  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *       @variable int[] tableaurandom sert a creer une variable avec une proposition random comme nombre.
     *       @variable  int[] tableauproposition contient la variable random qui sert apres dans les différents jeux.
     *       @variable int choix permet de faire un choix parmi les différents mode de jeu 1,2,3.
     *
     *
     *     @author thomas
     *
     */

    void jeucommence(int tabsolutionforce[], String resultat[], String resultatdefense[], int[] tableaurandom, int[] tableauproposition) {
        Logger.info("Le joueur fait son choix de jeu");
        Choixjeu Choixjeu = new Choixjeu();

        System.out.println("Bonjour bienvenue dans le jeu choisi ton mode de jeu");
        System.out.println();
        System.out.println("----------------------------------------------------");
        System.out.println("si tu veux le mode facile tape 1");
        System.out.println("----------------------------------------------------");
        System.out.println("si tu veux le mode defenseur tape 2");
        System.out.println("----------------------------------------------------");
        System.out.println("si tu veux le mode duel tape 3");
       char choix = sc.nextLine().charAt(0);
        Choixjeu.Choixjeugame(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, choix);
        while (Character.getNumericValue(choix) != 1 && Character.getNumericValue(choix) != 2 && Character.getNumericValue(choix) != 3) {
            Logger.info("le joueur n'a pas rentré un chiffre entre 1 et 3.");
            System.out.println("ce n'est pas un nombre");
            System.out.println("voulez -vous refaire une proposition ? si oui alors 'o' ou non alors 'n' ");
            Logger.info("On propose au joueur de refaire une proposition.");
            char ouiounon = sc.nextLine().charAt(0);
            while (ouiounon == 'o') {
                Logger.info("le joueur souhaite refaire une proposition est jouer a un des trois modes de jeux.");
                System.out.println("Bonjour bienvenue dans le jeu choisi ton mode de jeu");
                System.out.println();
                System.out.println("----------------------------------------------------");
                System.out.println("si tu veux le mode facile tape 1");
                System.out.println("----------------------------------------------------");
                System.out.println("si tu veux le mode defenseur tape 2");
                System.out.println("----------------------------------------------------");
                System.out.println("si tu veux le mode duel tape 3");
                System.out.println("----------------------------------------------------");
                choix = sc.nextLine().charAt(0);
                if (Character.getNumericValue(choix) != 1 && Character.getNumericValue(choix) != 2 && Character.getNumericValue(choix) != 3) {
                    Logger.info("le joueur n'a pas rentré un chiffre entre 1 et 3.");
                    System.out.println("ce n'est pas un nombre");
                    System.out.println("voulez -vous refaire une proposition ? si oui alors 'o' ou non alors 'n' ");
                    Logger.info("le joueur peut dire si il veut jouer ou non");
                    ouiounon = sc.nextLine().charAt(0);
                }
                if (ouiounon == 'n') {
                    Logger.info("le joueur a fait le choix que le jeu est fini");
                    System.out.println("au revoir et à bientot");
                    break;
                }
                Choixjeu.Choixjeugame(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, choix);
            }
            if (ouiounon == 'n') {
                Logger.info("le joueur a fait le choix que le jeu est fini");
                System.out.println("au revoir et à bientot");
                break;
            }
        }
    }
}
