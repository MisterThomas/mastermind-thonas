package com.oc.game;

import org.apache.logging.log4j.LogManager;

import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;

public class Plusoumoinsduel {
    Config Config = new Config();
    Properties prop = new Properties();
   // FileOutputStream filestream = null;
    int longCombi= Integer.getInteger(prop.getProperty("db.longueurCombinasion"));
    boolean verifdeveloppeur = Boolean.getBoolean(prop.getProperty("db.modeDeveloppeur"));
    Scanner sc = new Scanner(System.in);
    private static final org.apache.logging.log4j.Logger Logger = LogManager.getLogger();
    /**
     * class Plusoumoinsduel avec le mode 3 c'est l'ordi et le joueur  qui joue tour a tour.
     *
     *      @method  void attaque methode pour le mode 1 attaque  ou le joueur fait des propositions(reponses) avec en args tabsolutionforce,resultat,
     *
     *       @variable int[] tabsolutionforce est la combinastion secrete.
     *       @variable  String[] resultat est le resultat d'attaque.
     *       @variable  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *       @variable String reponse sert au joeur à mettre sa reponse pour devine  la tabsolutionforce.
     *
     *
     *      @method  void defenseur avec l'ordi qui joue à notre place.
     *
     *        @variable int[] tabsolutionforce est la combinastion secrete.
     *        @variable  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *        @variable int[] tableaurandom sert a creer une variable avec une proposition random comme nombre.
     *        @variable  int[] tableauproposition contient la variable random qui sert apres dans les différents jeux.
     *
     *
     *      @return formule decothomie getRandomInRange
     *
     *     @variable int tourjeu3 sert a definir le nombre de tour que se deroule la partie
     *
     *
     *     @author thomas
     *
     */

     int getRandomNumberInRange ( int min, int max){
        if (min > max) {
            Logger.error("le min est plus grand que le max.");
            throw new IllegalArgumentException("max must be greater than min");
        }


        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    /**
     * methode attaque avec tous les args dans la class main.
     * recherche en mode attaque
     *
     */
       public void attaque( int[] tabsolutionforce, String[] resultat, int loop) {
           if (loop == 0) {
               System.out.println("Bienvenue dans le jeu en mode 3 duel sous cette phrase. Vous pouvez entrer votre permiere proposition.");
               System.out.println("Bonjour voulez-vous jouer au mode developpeur ?");
               System.out.println("Si oui vous mettrez 'o' non 'n' ");
               char modedeveloppeur = sc.nextLine().charAt(0);

               while (modedeveloppeur == 'o'){
                   System.out.println("le mode developpeur est activé");
                   verifdeveloppeur = true;
                   System.out.println(tabsolutionforce);
                   System.out.println("--------------------------------");
                   System.out.println("Bienvenue dans le jeu 1 mode  attaquant sous cette phrase. Vous pouvez entrez votre premiere propostion");
               }
               while (modedeveloppeur == 'n') {
                   System.out.println("Vous jourez sans le mode developpeur");
                   System.out.println("---------------------------------");
                   System.out.println("Bienvenue dans le jeu 1 mode  attaquant sous cette phrase. Vous pouvez entrez votre premiere propostion");
               }
               while ((modedeveloppeur != 'n') && (modedeveloppeur != 'o')) {
                   System.out.println("vous n'avez pas rentrer une proposition valide.");
               }
           }
           Logger.info("le joueur fait une proposition");
           System.out.print("Vous avez tentez : ");
            String reponse = sc.nextLine();
           while (reponse.length() < longCombi || reponse.length() > longCombi ) {
               if(reponse.length() < longCombi){
                   System.out.println("vous n'avez pas assez de caractére pour votre reponse");
               }
               if(reponse.length() > longCombi){
                   System.out.println("vous avez trop de caractére pour votre reponse");
               }
           }
           while((Character.getNumericValue(reponse.charAt(0)) != 1) && (Character.getNumericValue(reponse.charAt(0)) != 2) && (Character.getNumericValue(reponse.charAt(0)) != 3) && (Character.getNumericValue(reponse.charAt(0)) != 4) && (Character.getNumericValue(reponse.charAt(0)) != 5) && (Character.getNumericValue(reponse.charAt(0)) != 6) &&(Character.getNumericValue(reponse.charAt(0)) != 7) && (Character.getNumericValue(reponse.charAt(0)) != 8) && (Character.getNumericValue(reponse.charAt(0)) != 9)){
               System.out.println("vous n'avez pas rempli un caratére autorisé pour le permiere caractére");
           }
           while((Character.getNumericValue(reponse.charAt(1)) != 1) && (Character.getNumericValue(reponse.charAt(1)) != 2) && (Character.getNumericValue(reponse.charAt(1)) != 3) && (Character.getNumericValue(reponse.charAt(1)) != 4) && (Character.getNumericValue(reponse.charAt(1)) != 5) && (Character.getNumericValue(reponse.charAt(0)) != 6) &&  (Character.getNumericValue(reponse.charAt(1)) != 7) && (Character.getNumericValue(reponse.charAt(1)) != 8) && (Character.getNumericValue(reponse.charAt(1)) != 9)){
               System.out.println("vous n'avez pas rempli un caratére autorisé pour le deuxieme caractére");
           }
           while((Character.getNumericValue(reponse.charAt(2)) != 1) && (Character.getNumericValue(reponse.charAt(2)) != 2) && (Character.getNumericValue(reponse.charAt(2)) != 3) && (Character.getNumericValue(reponse.charAt(2)) != 4) && (Character.getNumericValue(reponse.charAt(2)) != 5) && (Character.getNumericValue(reponse.charAt(2)) != 6)  && (Character.getNumericValue(reponse.charAt(2)) != 7) && (Character.getNumericValue(reponse.charAt(2)) != 8) && (Character.getNumericValue(reponse.charAt(2)) != 9)){
               System.out.println("vous n'avez pas rempli un caratére autorisé pour le troiseme caractére");
           }
           while((Character.getNumericValue(reponse.charAt(3)) != 1) && (Character.getNumericValue(reponse.charAt(3)) != 2) && (Character.getNumericValue(reponse.charAt(3)) != 3) && (Character.getNumericValue(reponse.charAt(3)) != 4) && (Character.getNumericValue(reponse.charAt(3)) != 5) && (Character.getNumericValue(reponse.charAt(3)) != 6)  && (Character.getNumericValue(reponse.charAt(3)) != 7) && (Character.getNumericValue(reponse.charAt(3)) != 8) && (Character.getNumericValue(reponse.charAt(3)) != 9)){
               System.out.println("vous n'avez pas rempli un caratére autorisé pour le quatrieme caractére");
           }
           while((Character.getNumericValue(reponse.charAt(0)) != 1) && (Character.getNumericValue(reponse.charAt(0)) != 2) && (Character.getNumericValue(reponse.charAt(0)) != 3) && (Character.getNumericValue(reponse.charAt(0)) != 4) && (Character.getNumericValue(reponse.charAt(0)) != 5) && (Character.getNumericValue(reponse.charAt(0)) != 6) && (Character.getNumericValue(reponse.charAt(0)) != 7) && (Character.getNumericValue(reponse.charAt(0)) != 8) && (Character.getNumericValue(reponse.charAt(0)) != 9) || (Character.getNumericValue(reponse.charAt(1)) != 1) && (Character.getNumericValue(reponse.charAt(1)) != 2) && (Character.getNumericValue(reponse.charAt(1)) != 3) && (Character.getNumericValue(reponse.charAt(1)) != 4) && (Character.getNumericValue(reponse.charAt(1)) != 5) && (Character.getNumericValue(reponse.charAt(0)) != 6) &&  (Character.getNumericValue(reponse.charAt(1)) != 7) && (Character.getNumericValue(reponse.charAt(1)) != 8) && (Character.getNumericValue(reponse.charAt(1)) != 9) || (Character.getNumericValue(reponse.charAt(2)) != 1) && (Character.getNumericValue(reponse.charAt(2)) != 2) && (Character.getNumericValue(reponse.charAt(2)) != 3) && (Character.getNumericValue(reponse.charAt(2)) != 4) && (Character.getNumericValue(reponse.charAt(2)) != 5) && (Character.getNumericValue(reponse.charAt(2)) != 6)  && (Character.getNumericValue(reponse.charAt(2)) != 7) && (Character.getNumericValue(reponse.charAt(2)) != 8) && (Character.getNumericValue(reponse.charAt(2)) != 9) || (Character.getNumericValue(reponse.charAt(3)) != 1) && (Character.getNumericValue(reponse.charAt(3)) != 2) && (Character.getNumericValue(reponse.charAt(3)) != 3) && (Character.getNumericValue(reponse.charAt(3)) != 4) && (Character.getNumericValue(reponse.charAt(3)) != 5) && (Character.getNumericValue(reponse.charAt(3)) != 6)  && (Character.getNumericValue(reponse.charAt(3)) != 7) && (Character.getNumericValue(reponse.charAt(3)) != 8) && (Character.getNumericValue(reponse.charAt(3)) != 9)){
               System.out.println("vous devez rentrer une nouvelle proposition");
               reponse = sc.nextLine();
           }
            for (int tour = 0; tour < 4; tour++) {
                if (Character.getNumericValue(reponse.charAt(tour)) == tabsolutionforce[tour]) {
                    resultat[tour] = "=";
                }
                if (Character.getNumericValue(reponse.charAt(tour)) > tabsolutionforce[tour]) {
                    resultat[tour] = "-";
                }
                if (Character.getNumericValue(reponse.charAt(tour)) < tabsolutionforce[tour]) {
                    resultat[tour] = "+";
                }
            }
           Logger.info("la variable du resultat joueur est affiché " + Arrays.toString(resultat));
           System.out.println("Résultat : " + Arrays.toString(resultat));
           System.out.println("---------------------");
                }
    /**
     * methode defenseur avec tous les args dans la class main.
     *il a une decothomie pour que l'ordi trouve lui meme la formule
     *
     *
     */
       public  void defenseur (int loop, int[] tabsolutionforce, int[] tableaurandom, String[] resultatdefense, int[] tableauproposition){
            Random r = new Random();
           for(int i = 0; i < 4; ++i) {
               if (loop == 1) {
                   tableaurandom[i] = r.nextInt(9) + 1;
               }
               tableauproposition[i] = tableaurandom[i];
           }

           for(int i = 0; i < 4; ++i) {
               if (tableauproposition[i] == tabsolutionforce[i]) {
                   resultatdefense[i] = "=";
                   tableaurandom[i] = tableauproposition[i];
               }

               if (tableauproposition[i] > tabsolutionforce[i]) {
                   resultatdefense[i] = "-";
                   tableaurandom[i] = getRandomNumberInRange(1, tableauproposition[i] - 1);
               }

               if (tableauproposition[i] < tabsolutionforce[i]) {
                   resultatdefense[i] = "+";
                   tableaurandom[i] = getRandomNumberInRange(tableauproposition[i] + 1, 9);
               }
           }
           Logger.info("la proposition de l'ordi et le resultatdefense" +Arrays.toString(tableauproposition) + " "+ Arrays.toString(resultatdefense));
           System.out.println("Vous avez tentez : " + Arrays.toString(tableauproposition));
           System.out.println("Résultat : " + Arrays.toString(resultatdefense));
           System.out.println("---------------------");
                }
            }
     //   }

     //   }
