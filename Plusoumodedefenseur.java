package com.oc.game;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;

public class Plusoumodedefenseur {
    Scanner sc = new Scanner(System.in);
    Config Config = new Config();
    Properties prop = new Properties();
    boolean verifdeveloppeur = Boolean.getBoolean(prop.getProperty("db.modeDeveloppeur"));
    private static final org.apache.logging.log4j.Logger Logger = LogManager.getLogger();

    /**
     * class Plusoumoinsdefenseur avec le mode 2 c'est l'ordi qui joue pour nous.
     *
     *      @method  void defenseur avec l'ordi qui joue à notre place.
     *
     *      @return formule decothomie getRandomInRange
     *
     *     @variable int tourjeu2 sert a definir le nombre de tour que se deroule la partie
     *     @variable  int[] tabsolutionforce est la combinastion secrete.
     *     @variable  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *     @variable int[] tableaurandom sert a creer une variable avec une proposition random comme nombre.
     *     @variable  int[] tableauproposition contient la variable random qui sert apres dans les différents jeux.
     *
     *     @author thomas
     *
     */

     int getRandomNumberInRange(int min, int max) {
        if (min > max) {
            Logger.error("le min est plus grand que le max.");
            throw new IllegalArgumentException("max must be greater than min");
        }


        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    /**
     * methode defenseur avec tous les args dans la class main.
     *il a une decothomie pour que l'ordi trouve lui meme la formule
     *
     *
     */
    public  void defenseur (int loop, int[] tabsolutionforce, int[] tableaurandom, String[] resultatdefense, int[] tableauproposition) {
        Random r = new Random();
        for (int i = 0; i < 4; i++) {
            if (loop == 0) {
                Logger.info("premiere fois que l'ordi rempli une proposition et choix du mode developpeur");
                tableaurandom[i] = r.nextInt(9) + 1;
                Logger.info("le joueur commence a joue et choisi le mode developpeur ou non");
                System.out.println("Bonjour voulez-vous jouer au mode developpeur ?");
                System.out.println("Si oui vous mettrez 'o' non 'n' ");
                char modedeveloppeur = sc.nextLine().charAt(0);

                while (modedeveloppeur == 'o'){
                    System.out.println("le mode developpeur est activé");
                    verifdeveloppeur = true;
                    System.out.println(tabsolutionforce);
                    System.out.println("Bienvenue dans le jeu en mode 2 defenseur sous cette phrase. Vous pouvez entrer votre premiere proposition.");
                }
                while (modedeveloppeur == 'n') {
                    System.out.println("Vous jourez sans le mode developpeur");
                    System.out.println("Bienvenue dans le jeu en mode 2 defenseur sous cette phrase. Vous pouvez entrer votre premiere proposition.");
                }
                while ((modedeveloppeur != 'n') && (modedeveloppeur != 'o')) {
                    System.out.println("vous n'avez pas rentrer une proposition valide.");
                }
            }
            Logger.info("les propostions faite par l'ordi");
            tableauproposition[i] = tableaurandom[i];
        }
            for (int i = 0; i < 4; i++) {
                if ((tableauproposition[i]) == tabsolutionforce[i]) {
                    resultatdefense[i] = "=";
                    tableaurandom[i] = tableauproposition[i];

                }
                if ((tableauproposition[i]) > tabsolutionforce[i]) {
                    resultatdefense[i] = "-";
                    tableaurandom[i] = getRandomNumberInRange(1, tableauproposition[i] - 1);
                }
                if ((tableauproposition[i]) < tabsolutionforce[i]) {
                    resultatdefense[i] = "+";
                    tableaurandom[i] = getRandomNumberInRange(tableauproposition[i] + 1, 9);
                }
            }
        Logger.info("la proposition de l'ordi et le resultatdefense" +Arrays.toString(tableauproposition) + " "+ Arrays.toString(resultatdefense));
        System.out.println("Vous avez tentez : " + Arrays.toString(tableauproposition));
        System.out.println("Résultat : " + Arrays.toString(resultatdefense));
        System.out.println("---------------------");
                }
            }