package com.oc.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.Scanner;

public class Choixjeu {

    Scanner sc = new Scanner(System.in);
    private static final org.apache.logging.log4j.Logger Logger = LogManager.getLogger();

    /**
     * class Choixjeu permet de faire le choix entre les methode et le mode de jeu 1,2,3 avec en args tous les variables des trois jeux.
     *
     *      @method void attaque methode pour le mode 1 attaque  ou le joueur fait des propositions(reponses) avec en args tabsolutionforce,resultat,
     *
     *      @variable int[] tabsolutionforce est la combinastion secrete.
     *      @variable  String[] resultat est le resultat d'attaque.
     *      @variable  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *      @variable String reponse sert au joeur à mettre sa reponse pour devine  la tabsolutionforce.
     *
     *      @method  void defenseur methode pour le jeu 2  avec l'ordi qui joue à notre place.
     *
     *      @variable  int[] tabsolutionforce est la combinastion secrete.
     *      @variable  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *      @variable int[] tableaurandom sert a creer une variable avec une proposition random comme nombre.
     *      @variable  int[] tableauproposition contient la variable random qui sert apres dans les différents jeux.
     *
     *       @method  void attaque methode pour le mode 3 attaque  ou le joueur fait des propositions(reponses) avec en args tabsolutionforce,resultat,
     *
     *       @variable int[] tabsolutionforce est la combinastion secrete.
     *       @variable  String[] resultat est le resultat d'attaque.
     *       @variable  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *       @variable String reponse sert au joeur à mettre sa reponse pour devine  la tabsolutionforce.
     *
     *
     *        @method  void defenseur avec l'ordi qui joue à notre place.
     *
     *        @variable int[] tabsolutionforce est la combinastion secrete.
     *        @variable  String[] resultatdefense est le resultat de l'ordi en mode defense.
     *        @variable int[] tableaurandom sert a creer une variable avec une proposition random comme nombre.
     *        @variable  int[] tableauproposition contient la variable random qui sert apres dans les différents jeux.
     *
     *     @variable  int tourjeu1, tourjeu2, tourjeu3 sert a jouer differents tour du jeu.
     *     @boolean correct sert a verifier si le joueur a gagné ou non si c'est true le joueur a gagné.
     *     @integer   boucle checkresultat1, checkresultat2, checkresultat3attaque, checkresultat3defnse sert a verifier si le boolean est bon en
     *     @char ouiounonrejouer1, ouiounonrejouer2, ouiounonrejouer3, sert a definir si le joueur veut jouer ou non
     *
     *
     *     @author thomas
     *
     */


    void Choixjeugame(int tabsolutionforce[], String resultat[], String resultatdefense[], int[] tableaurandom, int[] tableauproposition, char choix) {

        Plusoumoinsattaque Plusoumoinsattaque = new Plusoumoinsattaque();
        Plusoumodedefenseur Plusoumodedefenseur = new Plusoumodedefenseur();
        Plusoumoinsduel Plusoumoinsduel = new Plusoumoinsduel();
        Jeudemarrage Jeudemarrage = new Jeudemarrage();
        Config Config = new Config();
        Properties prop = new Properties();
        FileOutputStream filestream = null;
        boolean verifgame = Boolean.getBoolean(prop.getProperty("db.correct"));
        boolean verifgameAuto = Boolean.getBoolean(prop.getProperty("db.correctauto"));
        boolean verifgameDefense = Boolean.getBoolean(prop.getProperty("db.correctdefense"));

        if (Character.getNumericValue(choix) == 1) {
            Logger.info("l'utilisateur a fais le choix du permier jeu");
            for (int tourjeu1 = 0; tourjeu1 < 10; tourjeu1++) {
                Plusoumoinsattaque.attaque(tabsolutionforce, resultat, tourjeu1);
                boolean correct = true;
                for (int checkresultat1 = 0; checkresultat1 < resultat.length; checkresultat1++) {
                    if (resultat[checkresultat1] != "=") {
                        //correct = false;
                        verifgame =false;
                        break;
                    }
                }
                if ((correct)||(verifgame)) {
                    Logger.info("le joueur a gagné");
                    System.out.println("bravo vous avez gagné");
                    Logger.info("le joueur peut rejouer");
                    System.out.println("Voulez-vous rejouer à un jeu ?");
                    System.out.println("si oui mettez 'o' non mettez 'n'.");
                    char ouiounonrejouer1 = sc.nextLine().charAt(0);
                   if (ouiounonrejouer1 == 'o') {
                       Logger.info("le joueur rejoue");
                       Jeudemarrage.jeucommence(tabsolutionforce,resultat, resultatdefense,tableaurandom,tableauproposition);
                  }
                   else if (ouiounonrejouer1 == 'n') {
                       Logger.info("le joueur quitte le jeu");
                       System.out.println("au revoir et à bientot");
                       break;
                   }
                } else if (tourjeu1 == 9 && !verifgame) {
                    Logger.info("le joueur a perdu et la combinaison s'affiche");
                    System.out.println("vous avez perdu la combinastion etait :");
                    System.out.println(Arrays.toString(tabsolutionforce));
                    System.out.println("--------------------------------------");
                    Logger.info("le joueur peut rejouer");
                    System.out.println("Voulez-vous rejouer à un jeu ?");
                    System.out.println("si oui mettez 'o' non mettez 'n'.");
                    char ouiounonrejouer1 = sc.nextLine().charAt(0);
                    if (ouiounonrejouer1 == 'o') {
                        Logger.info("le joueur rejoue");
                        Jeudemarrage.jeucommence(tabsolutionforce,resultat, resultatdefense,tableaurandom,tableauproposition);
                    }
                    else if (ouiounonrejouer1 == 'n') {
                        Logger.info("Le joueur quitte le jeu");
                        System.out.println("au revoir et à bientot");
                        break;
                    }
                } else {
                    Logger.info("le joueur n'a pas gagné et fait une nouvelle proposition");
                    System.out.println("vous etes à tant de prospositions : " + (tourjeu1 + 1 + 1) + " votre resultat est: ");
                }
            }

        } else if (Character.getNumericValue(choix) == 2) {
            Logger.info("le joueur fait le choix du deuxieme jeu");
            for (int tourjeu2 = 0; tourjeu2 < 10; tourjeu2++) {
                Plusoumodedefenseur.defenseur(tourjeu2, tabsolutionforce, tableaurandom, resultatdefense, tableauproposition);
                boolean correct = true;
                for (int checkresultat2 = 0; checkresultat2 < resultatdefense.length; checkresultat2++) {
                    if (resultatdefense[checkresultat2] != "=") {
                        correct = false;
                        verifgame = false;
                        break;
                    }
                }
                if ((correct)||(verifgame)) {
                    Logger.info("l'ordi a gagné");
                    System.out.println("bravo l'ordi a gagné.");
                    Logger.info("le joueur peut rejouer");
                    System.out.println("Voulez-vous rejouer à un jeu ?");
                    System.out.println("si oui mettez 'o' non mettez 'n'.");
                    char ouiounonrejouer2 = sc.nextLine().charAt(0);
                    if (ouiounonrejouer2 == 'o') {
                        Logger.info("le joueur rejoue ");
                        Jeudemarrage.jeucommence(tabsolutionforce,resultat, resultatdefense,tableaurandom,tableauproposition);
                    }
                    else if (ouiounonrejouer2 == 'n') {
                        Logger.info("le joueur quitte le jeu");
                        System.out.println("au revoir et à bientot.");
                        break;
                    }
                } else if (tourjeu2 == 9 && !verifgame) {
                    Logger.info("le joueur a perdu et la combinaison s'affiche");
                    System.out.println("vous avez perdu la combinastion etait :");
                    System.out.println(Arrays.toString(tabsolutionforce));
                    System.out.println("--------------------------------------");
                    Logger.info("le joueur peut rejouer a un jeu");
                    System.out.println("Voulez-vous rejouer à un jeu ?");
                    System.out.println("si oui mettez 'o' non mettez 'n'.");
                    char ouiounonrejouer2 = sc.nextLine().charAt(0);
                    if (ouiounonrejouer2 == 'o') {
                        Logger.info("le joueur va rejouer");
                        Jeudemarrage.jeucommence(tabsolutionforce,resultat, resultatdefense,tableaurandom,tableauproposition);
                    }
                    else if (ouiounonrejouer2 == 'n') {
                        Logger.info("le joueur quitte le jeu");
                        System.out.println("au revoir et à bientot");
                        break;
                    }
                } else {
                    Logger.info("le joueur n'a pas trouver la conbinaistion durant sa proposition");
                    System.out.println("vous etes à tant de prospositions : " + (tourjeu2 + 1 + 1) + " votre resultat est: ");
                }
            }
        } else if (Character.getNumericValue(choix) == 3) {
            Logger.info("le joueur va joueur au jouer au mode trois");
            for (int tourjeu3 = 0; tourjeu3 < 10; tourjeu3++) {
                if (tourjeu3 % 2 == 0) {
                    Logger.info("le joueur joue en pair au mode attaque");
                    Plusoumoinsduel.attaque(tabsolutionforce, resultat, tourjeu3);
                } else {
                    Logger.info("l'ordi joue en mode impair");
                    Plusoumoinsduel.defenseur(tourjeu3, tabsolutionforce, tableaurandom, resultatdefense, tableauproposition);
                }

                boolean correctauto = true;
                for (int checkresultat3attaque = 0; checkresultat3attaque  < resultat.length; checkresultat3attaque ++) {
                    if (resultat[checkresultat3attaque] != "=") {
                       // correctauto = false;
                        verifgameAuto =false;
                        break;
                    }
                }
                  boolean correctdefense = true;
                  for (int checkresultat3defense = 0; checkresultat3defense < resultatdefense.length; checkresultat3defense++) {
                   if (resultatdefense[checkresultat3defense] != "=") {
                  //  correctdefense = false;
                    verifgameDefense =false;
                     break;
                   }
                  }
                        if ((correctauto)||(correctdefense)||(verifgameAuto)||(verifgameDefense)) {
                            if((correctauto)||(verifgameAuto)) {
                                Logger.info("joueur a gagné");
                                System.out.println("bravo vous avez gagné.");
                                System.out.println("Voulez-vous rejouer à un jeu ?");
                                System.out.println("si oui mettez 'o' non mettez 'n'.");
                                char ouiounonrejouer = sc.nextLine().charAt(0);
                                if (ouiounonrejouer == 'o') {
                                    Logger.info("Le joueur rejoue");
                                    Jeudemarrage.jeucommence(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition);
                                }else if (ouiounonrejouer == 'n') {
                                    Logger.info("le joueur va quitter le jeu");
                                    System.out.println("au revoir et à bientot");
                                    break;
                                }
                            }
                            if((correctdefense)||(verifgameDefense)){
                                Logger.info("l'ordi a gagné");
                                System.out.println("l'ordi a gagné.");
                                System.out.println("Voulez-vous rejouer à un jeu ?");
                                System.out.println("si oui mettez 'o' non mettez 'n'.");
                            }
                            char ouiounonrejouer3 = sc.nextLine().charAt(0);
                            if (ouiounonrejouer3 == 'o') {
                                Logger.info("Le joueur rejoue");
                                Jeudemarrage.jeucommence(tabsolutionforce,resultat, resultatdefense,tableaurandom,tableauproposition);
                            }
                            else if (ouiounonrejouer3 == 'n') {
                                Logger.info("Le joueur quitte le jeu");
                                System.out.println("au revoir et à bientot");
                                break;
                            }
                        } else if ((tourjeu3 == 9 && !verifgameAuto) ||(tourjeu3 == 9 && !verifgameDefense) ) {
                            Logger.info("le joueur a perdu et la combinaison s'affiche");
                            System.out.println("vous avez tous les deux perdus la combinastion etait :");
                            System.out.println(Arrays.toString(tabsolutionforce));
                            System.out.println("--------------------------------------");
                            Logger.info("le joueur peut rejouer");
                            System.out.println("Voulez-vous rejouer à un jeu ?");
                            System.out.println("si oui mettez 'o' non mettez 'n'.");
                            char ouiounonrejouer3 = sc.nextLine().charAt(0);
                            if (ouiounonrejouer3 == 'o') {
                                Logger.info("le joueur rejoue");
                                Jeudemarrage.jeucommence(tabsolutionforce,resultat, resultatdefense,tableaurandom,tableauproposition);
                            }
                            else if (ouiounonrejouer3 == 'n') {
                                Logger.info("le joueur quitte le jeu");
                                System.out.println("au revoir et à bientot");
                                break;
                            }
                        } else {
                            Logger.info("le joueeur n'a pas trouvé la bonne combinaison il faut une nouvelle proposition.");
                            System.out.println("vous etes à tant de prospositions :" + (tourjeu3 + 1 + 1) + " votre resultat est: ");
                        }
            }
        }
    }
}
