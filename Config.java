package com.oc.game;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {

    public void configpropertises() {

        Properties prop = new Properties();
        FileOutputStream filestream = null;

        try {
            filestream = new FileOutputStream("config.properties");
            prop.setProperty("db.database", "localhost");
            prop.setProperty("db.user", "user");
            prop.setProperty("db.password", "password");
            prop.store(filestream, (String) null);
        } catch (
                IOException var21) {
            var21.printStackTrace();
        } finally {
            if (filestream != null) {
                try {
                    filestream.close();
                } catch (IOException var20) {
                    var20.printStackTrace();
                }
            }

        }
    }
}
