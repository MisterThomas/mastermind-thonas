//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.oc.game;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * class Main avec deroulement du jeu au début .
 *
 *     @variable int tourjeu1,int tourjeu2,  int tourjeu3 sert a definir le nombre de tour que se deroule la partie
 *     @variable string choix faire un choix de jeu 1,2,3
 *     @variable char faire un fois entre 'o' et 'n' pour si on veut jouer ou non
 *     @method  void Choixjeu.Choixjeugame qui permet de faire des faire le choix de chaque methode des trois jeux.
 *     @variable  int[] tabsolutionforce est la combinastion secrete.
 *     @variable  String[] resultat est le resultat d'attaque.
 *     @variable  String[] resultatdefense est le resultat de l'ordi en mode defense.
 *     @variable int[] tableaurandom sert a creer une variable avec une proposition random comme nombre.
 *     @variable  int[] tableauproposition contient la variable random qui sert apres dans les différents jeux..
 *
 *     @author thomas
 */

public class Main {
    private static final Logger Logger = LogManager.getLogger();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Logger.info("demarrage du programme.");
        Config Config = new Config();
        Properties prop = new Properties();
       //FileOutputStream filestream = null;
       int test = Integer.getInteger(prop.getProperty("db.longueurCombinasion"));
        int[] tabsolutionforce = new int[]{1, 2, 3, 4};
        String[] resultat = new String[test];
        String[] resultatdefense = new String[test];
        new Plusoumoinsattaque();
        new Plusoumodedefenseur();
        new Plusoumoinsduel();
        Choixjeu Choixjeu = new Choixjeu();
        int[] tableaurandom = new int [test];

        int [] tableauproposition = new int[test];
        System.out.println("Bonjour bienvenue dans le jeu choisi ton mode de jeu");
        System.out.println();
        System.out.println("----------------------------------------------------");
        System.out.println("si tu veux le mode facile tape 1");
        System.out.println("----------------------------------------------------");
        System.out.println("si tu veux le mode defenseur tape 2");
        System.out.println("----------------------------------------------------");
        System.out.println("si tu veux le mode duel tape 3");
        Logger.info("le joueur le mode de jeu");
        char choix = sc.nextLine().charAt(0);
        Choixjeu.Choixjeugame(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, choix);

        while(Character.getNumericValue(choix) != 1 && Character.getNumericValue(choix) != 2 && Character.getNumericValue(choix) != 3) {
            System.out.println("ce n'est pas un nombre");
            System.out.println("voulez -vous refaire une proposition ? si oui alors 'o' ou non alors 'n' ");
            Logger.info("le joueur peut refaire une proppsition si il veut jouer ou non.");
            char ouiounon = sc.nextLine().charAt(0);
            while(ouiounon == 'o') {
                Logger.info("le joueur souhaite faire un e proposition entre 1 et 3 pour jouer.");
                System.out.println("Bonjour bienvenue dans le jeu choisi ton mode de jeu");
                System.out.println();
                System.out.println("----------------------------------------------------");
                System.out.println("si tu veux le mode facile tape 1");
                System.out.println("----------------------------------------------------");
                System.out.println("si tu veux le mode defenseur tape 2");
                System.out.println("----------------------------------------------------");
                System.out.println("si tu veux le mode duel tape 3");
                System.out.println("----------------------------------------------------");
                choix = sc.nextLine().charAt(0);
                if (Character.getNumericValue(choix) != 1 && Character.getNumericValue(choix) != 2 && Character.getNumericValue(choix) != 3) {
                    Logger.info("il a encore fait une proposition avec un nombre en dehors de 1 et 3.");
                    System.out.println("ce n'est pas un nombre");
                    System.out.println("voulez -vous refaire une proposition ? si oui alors 'o' ou non alors 'n' ");
                    ouiounon = sc.nextLine().charAt(0);
                }

                if (ouiounon == 'n') {
                    Logger.info("le joueur a fait le choix de quitter le jeu.");
                    System.out.println("au revoir et à bientot");
                    break;
                }

                Choixjeu.Choixjeugame(tabsolutionforce, resultat, resultatdefense, tableaurandom, tableauproposition, choix);
            }

            if (ouiounon == 'n') {
                Logger.info("le joueur a fait le choix de quitter le jeu.");
                System.out.println("au revoir et à bientot");
                break;
            }
        }

    }
}
